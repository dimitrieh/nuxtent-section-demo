module.exports = {
    content: [
        [
            'docs', 
            {
                page: '/docs/_page',
                permalink: '/docs/:slug',
                isPost: false,
                anchorLevel: 4,
                generate: [
                    'get',
                    'getAll'
                ]
            }
        ],
    ],
    api: {
        baseURL: process.env.BASE_URL || 'http://localhost:3000',
        browserBaseURL: process.env.BASE_URL || 'http://localhost:3000'
    }
}
