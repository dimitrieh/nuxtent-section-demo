module.exports = {
    /*
    ** Headers of the page
    */

    head: {
        title: 'Test',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Test' }
        ]
    },


    /*
    ** Customize the progress bar color
    */

    loading: { color: '#3B8070' },


    /*
    ** Build configuration
    */

    generate: {
        dir: 'public'
    }
}
